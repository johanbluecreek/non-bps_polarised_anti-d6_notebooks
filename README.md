# Non-BPS polarised anti-D6 notebooks

This repository contains the details and calculations relevant for the numerical solutions that are a part of [https://arxiv.org/abs/1907.05295](https://arxiv.org/abs/1907.05295), by J. Blåbäck, F. F. Gautason, A. Ruipérez, T. Van Riet.

There are two notebooks present in this repository. The first is a Mathematica notebook (`non-BPS_analytical_expressions.nb`) that derives the analytical expressions used for the numerical searches. The second is a Jupyter notebook (`non-BPS_numerical_solutions.ipynb`) that runs Julia, that is written to be pedagogic and outlines the strategy for finding the non-BPS solution presented in the above article. The notebooks are also shipped in `.pdf` form for those who do not have readers for these notebooks.

## Dependencies

The Mathematica notebook depends on the package `diffgeo.m` by M. Headrick, which at the time of writing is hosted on

[http://people.brandeis.edu/~headrick/Mathematica/](http://people.brandeis.edu/~headrick/Mathematica/)

which has been archived on the Internet Archive Wayback Machine

[https://web.archive.org/web/20190301000000*/http://people.brandeis.edu/~headrick/Mathematica/diffgeo.m](https://web.archive.org/web/20190301000000*/http://people.brandeis.edu/~headrick/Mathematica/diffgeo.m)

This package is not distributed together with this notebook, and paths in this notebook that point to this package needs to be changed in order for the notebook to be able to execute commands the way they are intended.

The Mathematica notebook also uses a version of `diffego.m` that has its conventions changed to agree with [https://arxiv.org/abs/0907.2041](https://arxiv.org/abs/0907.2041).

## License

This is free and unencumbered software released into the public domain. See `LICENSE` file for details.
